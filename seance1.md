# Séance n°1 : base du langage

## Point d'entrée du programme

Le programme le plus court en C++ :

```cpp
int main() {
}
```

La syntaxe `T id() { ... }` où `T` est un type, et `id` un nom, permet de *définir* une fonction (retournant « `T` »).
Dans le code ci-avant, on définit donc la fonction `main` retournant un entier (type `int`)[^return:main].
Cette fonction est le point d'entrée (par défaut) d'un programme en C++.

On peut vérifier que cela fonctionne effectivement.
Pour cela, on peut créer un fichier `main.cpp` et y écrire le code ci-avant.
Pour produire l'exécutable, on utilisera la commande `g++ main.cpp`[^man:g++].
L'exécutable généré s'appelle `a.out`[^a.out] et peut être exécuté en tapant `./a.out`[^tab].
... Mais rien ne se passe.
Ce qui en l'occurrence est bien normal : la fonction `main` étant vide, le programme n'a rien à faire.

S'il est intéressant de voir le bon fonctionnement d'un programme, il est également important de voir lorsque cela échoue.
Pour nous convaincre que la fonction `main` est bien obligatoire, par exemple, il suffit d'écrire un fichier vide :small_orange_diamond:,
ou de changer le nom de la fonction :small_orange_diamond:.
N'hésitez pas, pour chaque exercice, à tester par vous-même des cas différents, y compris lorsque vous pensez que cela ne fonctionnera pas.

[^return:main]: La fonction `main` est très particulière, en plus d'être le point d'entrée du programme (en réalité, pour cette raison),
l'instruction `return` qui permet d'indiquer quelle valeur la fonction renvoie est optionnelle.
Si elle est omise, le compilateur complète par défaut avec `return 0;`.
Il est en revanche indispensable que cette fonction retourne un `int` :small_blue_diamond:.

[^man:g++]: La page de manuel de `g++` (`man g++`) n'est pas la plus facile à lire, il peut être pertinent de s'exercer sur d'autres pages au préalable.
En attendant, les options pertinentes seront fournies au fur et à mesure, non sans explications.

[^a.out]: Il s'agit d'un nom historique, lié au nom du format d'exécutables [a.out](https://fr.wikipedia.org/wiki/A.out) utilisé dans les premières versions d'Unix.

[^tab]: N'hésitez pas à utiliser la touche de tabulation aussi souvent que possible !
Ici, `./<tab>` suffit déjà. Et `g++ <tab>` suffisait également pour la commande précédente. :small_blue_diamond:

## Entrées et sorties

Un programme sans interaction avec l'utilisateur est rarement utile.
Or, jusqu'ici, nous avons fait un programme qui n'a *point d'entrée* ni de sortie.

Une syntaxe proposée par le C++ pour les entrées et sorties :small_blue_diamond: repose sur des outils fournis par une bibliothèque : `iostream`[^iostream].
Pour connaître ce que *déclare* cette bibliothèque, il est nécessaire de l'inclure dans notre programme. :small_orange_diamond:

```cpp
#include <iostream>

int main() {
}
```

(N'oubliez pas de tester également des cas censés ne pas fonctionner.
Par exemple, ici, une possibilité est de faire une faute dans le nom de la bibliothèque)

La documentation pertinente à propos de `iostream` se trouve sur [cppreference](https://en.cppreference.com/w/cpp/io).
Cependant, dans un premier temps, il est pertinent de se contenter de savoir qu'il existe deux outils principaux :
- [`std::cin`](https://en.cppreference.com/w/cpp/io/cin) (_**c**haracter **in**put_) pour l'entrée standard ;
- [`std::cout`](https://en.cppreference.com/w/cpp/io/cout) (_**c**haracter **out**put_) pour la sortie standard.

### Sorties

La syntaxe des flux du C++ (dont `std::cin` et `std::cout` font partie) utilise les symboles `>>` et `<<`.
Dans un premier temps, nous mettons de côté `std::cin` pour nous concentrer sur `std::cout` :

```cpp
std::cout << "Hello, world!\n";
```

Ce code peut être ajouté dans l'exemple fonctionnel précédent[^syn:indent] afin d'obtenir un programme affichant `Hello, world!` suivi d'un retour à la ligne. :small_blue_diamond:

### Entrées

La syntaxe pour les entrées (par exemple avec `std::cin`) est similaire et utilise à la place `>>`.
Cependant, pour pouvoir l'utiliser, il est nécessaire de disposer d'un mécanisme pour conserver une information...

[^iostream]: Pour **i**nput **o**utput **stream**, soit, en français, flux d'entrée/sortie.

[^syn:indent]: N'oubliez pas l'indentation. :small_blue_diamond:

## Variables

La notion de variable est fondamentale dans tout langage de programmation.
Il s'agit :
- d'un nom, pour pouvoir s'y référer ;
- d'un espace en mémoire, pour conserver une information ;
- d'un type associé, pour savoir comment interpréter la donnée et quelle taille elle occupe.

On peut *définir* une variable avec la syntaxe `T id` où `T` est son type, et `id` son nom.
Les types possibles sont infinis, mais il existe des [types fondamentaux](https://en.cppreference.com/w/cpp/language/types) :
- `void` : type n'ayant aucune valeur possible ;
- `bool` : type pouvant prendre deux valeurs, `false` et `true` ;
- `char` : capable de conserver des entiers (petit) ;
- `int` : autre type pour conserver des entiers (plus grand) ;
- `float` : type permettant de conserver des nombres flottants ([IEEE754](https://en.wikipedia.org/wiki/IEEE_754)).

Cette liste n'est pas exhaustive mais donne une première impression de ce que sont les types fondamentaux.
Remarque importante : puisque `void` n'a aucune valeur possible, il n'est pas permis de créer des variables de ce type. :small_orange_diamond:

```cpp
int var; // valeur encore inconnue

var = 5; // un unique = symbolise l'affectation

std::cout << var << '\n'; // affiche 5, suivi d'un retour à la ligne
```

:arrow_forward: Écrivez un programme qui affiche le résultat d'un [calcul sur deux variables](https://en.cppreference.com/w/cpp/language/operator_arithmetic) :small_blue_diamond:.

### `std::cin`

On peut maintenant lire une valeur :small_orange_diamond: :
```cpp
std::cin >> var;
```

:arrow_forward: Écrivez un programme qui demande à un utilisateur deux nombres et affiche le résultat d'un calcul sur ceux-ci.

## Fonctions

La notion de fonction est également indispensable en programmation puisqu'elle permet, notamment, d'éviter de répéter du code pour l'utiliser plusieurs fois.
Une fonction possède ainsi, à l'instar d'une variable, un nom, mais à celui-ci on associe cette fois une suite d'instructions.

On *définit* une fonction ainsi :
```cpp
T id(...) {
	...
}
```

Avec `T` le type de retour de la fonction, `id` le nom de la fonction et :
- au sein des parenthèses, d'éventuels paramètres pour la fonction ;
- au sein des accolades, les instructions que la fonction exécutera lorsqu'elle sera appelée.

Les paramètres correspondent à des variables, locales à la fonction, dont la valeur sera celle des arguments passés à la fonction lors de son appel.
Par exemple :
```cpp
void f(int x) {
	std::cout << x;
}

int main() {
	f(5);
	f(7);
}
```

La fonction `f` prend un paramètre de type `int`, nommé `x`.
Lors du premier appel effectué dans la fonction `main`, l'argument donné à `f` est `5`, ainsi la fonction `f` s'exécute avec `x` initialisé à `5`.
Au second appel, `x` vaudra `7` au sein de la fonction `f`.

:arrow_forward: Écrivez une fonction (correctement nommée) qui effectue une opération arithmétique sur ses deux paramètres et retourne le résultat.
Vous pouvez ensuite l'utiliser pour mettre à jour votre programme précédent.
Rappel : à l'exception de la fonction `main`, si votre fonction possède un type de retour autre que `void`, elle **doit** atteindre une instruction `return ...;`.

## Git

[Git](https://fr.wikipedia.org/wiki/Git) est un logiciel de gestion de versions, c'est-à-dire qu'il permet :
- de sauvegarder votre travail, progressivement ;
- de voir l'état passé de votre travail, d'y revenir, ou d'annuler plus précisément des modifications ;
- de gérer le travail à plusieurs ;
- associé à une plateforme telle que GitLab, de facilement accéder à ce travail ;
- toujours grâce à GitLab ou équivalent, de gérer des droits entre plusieurs personnes ayant besoin de travailler sur le projet.

### Initialisation

#### Localement d'abord

Créer un dépôt Git se fait très simplement :
```sh
git init
```

À exécuter en étant dans le dossier concerné.

Il faut ensuite configurer le distant (après l'avoir créé sur GitLab) avec `git remote add origin <URL>`.
En créant d'abord le projet sur GitLab, cette procédure est un peu simplifiée.

#### Sur GitLab d'abord

Après avoir créé le projet sur GitLab, on peut le cloner localement, ce qui revient à l'initialiser et configurer le distant.

```sh
git clone git@gitlab.com:...
```

### Commits

Ensuite, il s'agit d'indiquer à Git quelles modifications doivent être sauvegardées en les *ajoutant* :
```sh
git add ...
```

Où `...` est une liste de fichiers contenant des modifications[^git:addrm].

Ensuite, après avoir ajouté toutes les modifications pertinentes, il faut les valider pour créer un « commit », une image :
```sh
git commit
```

Telle qu'elle, cette commande ouvrira un éditeur (celui configuré, un par défaut sinon[^git:vi]) au sein duquel on peut saisir un message à associer au commit.
Ce message est important car il permet de savoir (`git log` :small_blue_diamond:) quelles modifications le commit contient sans l'explorer entièrement.

### Synchronisation

Les commits sont créés localement, pour les partager, il faut ensuite les envoyer au distant :
```sh
git push
```

La première fois, Git indiquera une procédure pour déterminer comment lier le local au distant.
Il n'est pas nécessaire de faire un `push` pour chaque `commit`[^git:push].
En revanche, faites le au moins à la fin de chaque séance.

Pour récupérer des modifications faites autre part (si vous voulez travailler chez vous, par exemple), la commande réciproque est `git pull`.

[^git:addrm]: Notamment, si un fichier est supprimé, il s'agit bien d'une *modification* de son contenu, et pour indiquer à Git d'appliquer cette suppression,
`git add fichier_supprimé` fait ce qu'il faut.

[^git:vi]: Il est probable que l'éditeur ouvert soit `vi` (ou `vim`).
Si vous ne connaissez pas, il est conseillé d'en configurer un autre, ou bien de taper `git commit -m "le message du commit"`, cette seconde option étant plus limitée.
Si vous avez déjà lancé cet éditeur, le minimum à savoir est de taper `i` pour entrer en mode d'**i**nsertion, taper le message, Echap et `:x` + entrée pour sauvegarder et quitter.
:small_blue_diamond:

[^git:push]: Faites-le quand même si vous avez peur d'oublier.

### GitLab et clés SSH

Pour les opérations de synchronisation, il faut s'identifier auprès de GitLab.
Deux options sont possibles :
- utiliser une URL en HTTPS et saisir les informations de connexion à chaque synchronisation ;
- utiliser une URL en git@ (utilisant SSH) et utiliser un couple de clés SSH.

Pour ce second cas, il faut :
- générer un couple de clés avec par exemple `ssh-keygen -t ed25519` ;
- donner la partie publique à GitLab.

Remarque : sur les machines de TP, votre homedir n'est pas partagé d'une machine à l'autre.
Il faut donc que vous placiez votre couple de clés dans votre dossier partagé.

```sh
ssh-keygen -t ed25519 -f '~/SCIENCES/<dossier partagé>/id_ed25519'
```

Ensuite, il faut utiliser à chaque git pull/git push/git clone la bonne clé SSH.
Pour ce faire, pour la plupart des options, il faut modifier un fichier local à la machine, dans le homedir.
Cette opération est donc à faire à chaque fois que l'on change de machine.

Une solution ne nécessitant pas de modification locale, et que l'on va préférer, est la suivante :
```
export GIT_SSH_COMMAND='ssh -i ~/<dossier partagé>/id_ed25519'
```

Ceci doit être fait dans chaque terminal où vous voulez faire du git pull/git push, une seule fois.

## Const

Un mot-clé particulièrement important en C++ est `const`.
Généralement, il permet d'indiquer que ce à quoi il s'applique est « constant ».
Cette notion va être détaillée au fur et à mesure du cours, mais dans un premier temps, nous l'appliquerons aux variables.

On peut *définir* une variable constante ainsi :
```cpp
T const id = value;
```

Par rapport à la définition de variable vue précédemment, la seule différence est l'ajout du mot-clé `const`.
Celui-ci se place après ce qu'il qualifie de « constant » (à droite) :small_blue_diamond:.
Si on le lit en anglais : « id is a constant T » ; c'est assez naturel à partir du moment où l'on accepte de lire de droite à gauche.

Ce mot-clé indique au compilateur qu'il faut interdire toute modification ultérieure de la variable :small_orange_diamond:.
C'est pour cette raison que l'on va également initialiser la variable dès sa définition.
0
Pour le moment, l'intérêt peut vous paraître limité, mais grâce à ce mécanisme, le C++ permet d'être précis sur les intentions d'une portion de code.
La section suivante présente un cas concret de cela.

## Références

Le C++ propose un mécanisme d'alias pour les variables : grâce à celui-ci, on peut utiliser une variable selon différents noms.
Pour créer une référence, la syntaxe est la suivante :
```cpp
T id;
T& r = id;
```

Où `T` est un type quelconque (à l'exception, évidemment, de `void`).
La variable `r` est alors une référence sur la variable `id` : utiliser l'une ou l'autre est strictement identique.
Notamment :
```cpp
r = 5;
// à partir d'ici, id vaut 5
```

Si l'on veut, on peut créer des références sur des instances sans vouloir pouvoir les modifier :
```cpp
T const& cr = id;
// cr = 5; // ne compile pas
```

:arrow_forward: : En utilisant des références directement dans la fonction `main`, refaites un programme calculant un résultat à partir de deux valeurs.

:arrow_forward: : Écrivez ce même programme en passant par une fonction, laquelle accepte des références en paramètre plutôt que des valeurs. :small_blue_diamond:

:arrow_forward: : Écrivez une fonction `swap` capable d'intervertir la valeur de ses deux paramètres. :small_blue_diamond:

## Déclarations et définitions

Dans ce document, les termes déclaration et définition ont été utilisés précisément, et leur sens est important.
Nous n'avons pour le moment vu que les variables et les fonctions, mais le tableau qui suit sera complété plus tard.

|  élément  |  déclaration  |  définition     |
|-----------|---------------|-----------------|
| variable  | extern T id;  | T id;           |
| fonction  | T id(...);    | T id(...) {...} |

La déclaration d'une variable se fait assez rarement, en revanche, la déclaration de fonctions nous sera très utile.
Jusqu'à présent, tout le code produit tient aisément en un seul fichier (`main.cpp`) mais il sera rapidement
nécessaire de séparer en unités logiques, et dès lors il sera indispensable de produire des fichiers qui déclarent
les fonctions dont les définitions se trouvent autre part.

Pour illustrer, on peut reprendre un exemple quelconque pour lequel on a au moins une fonction.
:arrow_forward: Créez un nouveau fichier d'extension `cpp` et déplacez-y la définition de la fonction.

La ligne de commande habituelle (`g++ main.cpp`) devrait alors échouer.
En effet, `g++` ne connaît pas la fonction que vous essayez d'appeler : il faut la lui déclarer.
Pour cela, on peut simplement ajouter la ligne de déclaration correspondante avant la fonction `main`.

Habituellement, plutôt que d'écrire les déclarations dans chacun des fichiers où l'on désire utiliser des fonctions,
on utilise `#include` pour récupérer des déclarations inscrites dans des fichiers spécifiques : des fichiers d'en-tête.
Ces fichiers, d'extension `hpp`, auront la structure suivante :

```cpp
#ifndef PROJET_FICHIER_HPP
#define PROJET_FICHIER_HPP

// déclarations de fonctions

#endif
```

Où `PROJET_FICHIER_HPP` est un nom au sein duquel il convient de remplacer `PROJET` par le nom du projet actuel et `FICHIER` par le nom du fichier actuel.
Cette syntaxe avec `#ifndef`, `#define` et `#endif` est appelée « *header guards* » et permet d'éviter certains problèmes dans une circonstance particulière.
Nous verrons cela dans la section suivante.

:arrow_forward: Créez le fichier d'en-tête correspondant et incluez-le dans `main.cpp` pour résoudre le problème rencontré.

... Malheureusement, une nouvelle erreur (sauvage) apparaît.
En lisant attentivement, on apprend que cette fois, la **définition** n'est pas trouvée.

On peut alors résoudre le problème en utilisant la commande `g++ main.cpp x.cpp` où `x.cpp` est le nom de votre
second fichier.
Cette solution en revanche n'est pas celle que l'on va utiliser, comme nous le verrons dans la section suivante.
C'est-à-dire maintenant.

## Compilation et édition des liens

Jusqu'ici, la génération d'un exécutable s'est faite en une seule instruction :
```sh
g++ main.cpp
```

Cette seule commande regroupe deux :small_blue_diamond: notions distinctes :
- la compilation ;
- l'édition des liens.

La compilation consiste en l'interprétation d'un code source (dans notre cas en C++) pour générer des fichiers binaires.
Ces fichiers (d'extension `.o`) contiennent les *définitions*.
Pour cela, le compilateur a besoin de connaître les *déclarations*, généralement fournies par des fichiers d'en-tête.

Ainsi, une erreur selon laquelle on apprend qu'un symbole donné est inconnu indique clairement une déclaration manquante.
Une telle erreur est donc produite par le *compilateur* et se résout en ajoutant la déclaration (par exemple en n'oubliant pas le `#include` correspondant).

En revanche, après avoir produit les `.o`, il convient de les assembler pour générer un exécutable :small_blue_diamond:.
Cette tâche revient à l'éditeur de liens qui va vérifier, notamment, qu'à chaque appel de fonction correspond sa définition.
S'il ne trouve pas la définition, il échoue, et s'il trouve trop de définitions pour une même fonction, il échoue.

Le schéma ci-dessous illustre un cas concret de projet avec plusieurs fichiers :

```mermaid
graph TD
	common.hpp(common.hpp)

	a.hpp(a.hpp)
	a.cpp(a.cpp)
	a.o(a.o)

	b.hpp(a.hpp)
	b.cpp(b.cpp)
	b.o(b.o)

	main.cpp(main.cpp)
	main.o(main.o)

	common.hpp -->|#include| a.hpp
	common.hpp -->|#include| b.hpp

	a.hpp -->|#include| a.cpp -->|compilation| a.o
	a.hpp -->|#include| main.cpp

	b.hpp -->|#include| b.cpp -->|compilation| b.o
	b.hpp -->|#include| main.cpp

	main.cpp -->|compilation| main.o

	x(exécutable)
	a.o -->|édition des liens| x
	main.o -->|édition des liens| x
	b.o -->|édition des liens| x
```

On appelle « unité de compilation » un fichier d'extension `cpp` avec les fichiers d'en-tête inclus.
Dans cet exemple, le fichier `common.hpp` est inclus par `a.hpp` et `b.hpp`, eux-mêmes inclus dans `main.cpp`.
Cela signifie que sans artifice particulier, `common.hpp` serait doublement inclus dans `main.cpp`.
C'est à cela, et uniquement à cela, que servent les « header guards ».

Lorsque l'on écrit `g++ main.cpp a.cpp b.cpp` avec cet exemple, on demande à `g++` de :
- générer `main.o`, `a.o` et `b.o` ;
- appeler l'éditeur de liens sur ces fichiers pour produire l'exécutable ;
- supprimer les fichiers `.o`.

Une partie du travail est donc gâchée : la création des fichiers binaires est refaite entièrement à chaque fois.
Or, si seul le fichier `a.cpp` change, il semble inutile de générer de nouveau `main.o` et `b.o`.

### Compilation séparée

Pour résoudre ce problème, on peut compiler de manière séparée :
- `g++ -o main.o -c main.cpp` ;
- `g++ -o a.o -c a.cpp` ;
- `g++ -o b.o -c b.cpp` ;
- `g++ -o executable main.o a.o b.o`.

Si l'on modifie un fichier `.cpp`, il convient alors d'exécuter à nouveau la ligne de *compilation* correspondante,
puis la ligne finale qui fait appel à l'édition des liens.
Dans ces lignes de commande :
- `-o` permet d'indiquer où doit être sauvegardée la sortie (avec un argument ensuite) ;
- `-c` permet d'indiquer qu'il ne faut effectuer que la *compilation*.

Sans `-c`, pour tout fichier en entrée d'extension `cpp`, une compilation est effectuée,
et ensuite, tous les fichiers d'extension `o`, générés ou fournis dans la ligne de commande,
sont liés ensemble par appel à l'éditeur de liens.

Ces opérations sont répétitives, et on n'a même pas encore abordé quelques options capitales de `g++` que nous
devrons systématiquement ajouter par la suite !

### Make

Pour cela, il existe des systèmes permettant d'automatiser la compilation et l'édition des liens.
Plus généralement, ces systèmes permettent d'automatiser n'importe quelles instructions « génératrice ».

:arrow_forward: En utilisant un des codes sources d'exercice précédent n'ayant qu'un fichier `main.cpp`, essayez de taper : `make main`.

Sans rien lui fournir, `make` est capable de déduire comment produire un exécutable.
En revanche, dans le cas général, il est nécessaire de donner quelques instructions afin de paramétrer cela.
Ces instructions sont conservées dans un fichier `Makefile`.

#### Exemple de Makefile

Remarque : cet exemple est minimaliste, légèrement incomplet, et ne correspond pas à une attente du cours.
Vous pouvez lire cette partie sur l'exemple si cela vous intéresse.
Au cours de la séance suivante, un outil de plus haut niveau sera introduit pour générer automatiquement ces fichiers.

Un exemple de `Makefile` :
```makefile
CXXFLAGS=-Wall -Wextra
LDFLAGS=

.PHONY:
all: executable

executable: main.o foo.o
	$(CXX) -o $@ $^ $(LDFLAGS)

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -o $@ -c $<

.PHONY:
clean:
	rm *.o
```

En tapant simplement `make`, le programme lit le `Makefile` et trouve la première « règle » : `all`.
Celle-ci est précédée de `.PHONY` indiquant que le nom de la règle ne se rapporte pas à celui d'un fichier.

Une règle se décompose en trois parties : son nom, la liste de ses dépendances et la liste des actions à effectuer.
Pour `all`, on voit qu'elle dépend de `executable`.
Sa liste d'actions est vide.

La règle `executable` dépend de `main.o` et `foo.o`.
Si ces dépendances sont modifiées, alors elle exécutera `$(CXX) -o $@ $^ $(LDFLAGS)`.
En Makefile, une variable s'utilise avec la syntaxe `$(nom)` où `nom` est son nom.
`CXX`, `CXXFLAGS` et `LDFLAGS` font partie des variables fondamentales de Make, et en particulier,
`CXX` correspond à un compilateur C++ de la machine.
`CXXFLAGS` est utilisée pour conserver les options à transmettre au *compilateur*, tandis que `LDFLAGS` sert à définir les options pour l'*éditeur de liens*.
Quelques variables spéciales existent, `$@` indique le nom de la règle en cours, `$^` la liste des dépendances et `$<` la première de la liste.
