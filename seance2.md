# Seance n°2 : programmation orientée objet

Cette deuxième séance a pour objectif d'aborder la programmation orientée objet tout en commençant le projet.
Nous verrons la syntaxe pour la création d'une classe en C++ avec ses variables et fonctions membres.
Nous verrons également comment modifier la visibilité de ces éléments (*encapsulation*).

Nous utiliserons ici [CMake](https://cmake.org/) pour la première fois.

## CMake

CMake est un générateur de [systèmes de compilation](https://cmake.org/cmake/help/latest/manual/cmake-generators.7.html).
Il permet de n'avoir qu'une seule configuration pour différents systèmes de compilation.
On n'utilisera ici que les makefiles mais CMake pourrait générer une configuration pour [Ninja](https://ninja-build.org/) par exemple (ou même Visual Studio sous Windows).
CMake permet également de gérer plus simplement des projets dans lesquels plusieurs cibles (exécutables ou bibliothèques) sont créées.

Vous pouvez télécharger le contenu de ce dépôt git : https://gitlab.com/uca_edu/ipcli.
Plusieurs fichiers `CMakeLists.txt` sont présents dans le dossier `ipcli` et ses sous-dossiers. 
Ces fichiers seront à adapter pour ajouter les fichiers de sources que vous créerez.

```cmake
set(_src_rdossier
    fichier.cpp
)

set(_hdr_dossier
    fichier.hpp
)
```

Il est préférable lorsqu'on utilise CMake de créer un dossier `build` pour que CMake y place les fichiers produits sans les mélanger aux sources du projet. Le dossier build à la racine du projet est déjà ajouté au `.gitignore`, il ne sera donc pas pris en compte lors de vos commits.

```bash
mkdir build
cd build
```

Depuis le dossier `build`, on peut demander à CMake de générer le système de compilation.

```bash
cmake .. # Ici .. représente le chemin d'accès au CMakeLists.txt de plus haut niveau.
```

Vous pouvez ensuite directement compiler et éditer les liens.

```bash
make
```

Si le projet compile correctement, votre environnement de développement est prêt pour réaliser le projet. Vous pouvez alors exécuter le programme :

```bash
./ipcli/ipcli
```

Le programme lit une image `input.png` se trouvant à la racine du projet et produit l'image `output.bmp`.

## Objet

Un objet est un conteneur permettant de rassembler des données sous la forme de variables membres (également appelées attributs) et des comportements avec des fonctions membres (également appelées méthodes).

On définit une classe de cette manière :small_orange_diamond::

```cpp
class FirstClass {
};
```

Les variables et fonctions déclarées à l'intérieur de la classe sont dits *membres*. 

:arrow_forward: Modifiez le fichier `ipcli/ipcli.cpp` pour y ajouter une classe `ColorScale` (notre premier filtre) permettant de contenir trois nombres décimaux qui correspondront respectivement à un multiplicateur pour les canaux RGB de chaque pixel de l'image. Vous pouvez également déclarer une variable `colorScaleFilter` de ce nouveau type, dans la fonction `main`.

L'un des fondements du paradigme de programmation orientée objet est l'[**encapsulation**](https://www.larousse.fr/dictionnaires/francais/encapsulation/29088#:~:text=%20encapsulation&text=Opération%20permettant%20d%27enrober%20un,ci%20contre%20les%20influences%20extérieures.).
Ce principe a pour objectif de séparer la manipulation d'un objet (grâce à ses fonctions membres) de sa manière de fonctionner (ses variables membres).
De cette manière l'utilisateur de l'objet n'a pas besoin de se soucier de comment ce dernier a été conçu.
De plus, en accédant directement aux variables membres, l'utilisateur pourrait mettre l'objet dans un état[^objet:etat] invalide.  

:arrow_forward: Ajoutez trois fonctions membres permettant d'obtenir les valeurs stockées dans chacun des canaux :small_orange_diamond:.

---

[^objet:etat]: L'état d'un objet est défini par les valeurs de l'ensemble de ses variables membres.

## Visibilité

La visibilité permet de restreindre l'accès à un membre.
Plusieurs niveaux sont disponibles en C++.
La restriction la plus forte est lorsque les membres sont privés (`private`) : les membres ne sont accessibles que depuis la classe.
On peut rendre les membres accessibles depuis l'extérieur de l'objet grâce à la visibilité publique (`public`).

Pour changer la visibilité on procède comme cela :

```cpp
class FirstClass {
private:
    // éléments privés
public:
    // éléments publics    
};
```

Afin de garantir l'encapsulation, on essaiera de toujours déclarer privé les variables membres. Les méthodes permettant d'y accéder auront alors une visiblité plus permissive. (cf. https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines#Rc-private [^coreguidelines]).


:arrow_forward: Corrigez la visibilité pour pouvoir accéder aux trois fonctions membres depuis l'extérieur.
Ne vous attardez ici qu'à la compilation du programme et non à son exécution.

Il est possible d'avoir plusieurs sections de même visibilité, et leur ordre n'a pas d'importance :small_orange_diamond:. 

---

[^coreguidelines]: Ce site anglophone référence des bonnes pratiques pour le développement C++.

## Accesseurs

Les accesseurs sont des fonctions membres permettant d'accéder à une partie de l'état d'un objet.

On a déjà fait ici trois accesseurs permettant de lire les valeurs contenues dans les variables membres de `ColorScale`.
Ces accesseurs permettant la lecture d'une valeur sont appelés *getter*.
Leur nom est généralement préfixé de `has` ou `is` lorsqu'on veut connaître la valeur d'un booléen ou de `get` pour tout autre type de données.
Un *getter* est une fonction membre qui souvent ne modifie pas l'objet sur lequel elle est appelée.

:arrow_forward: Rendez la variable `colorScaleFilter` constante :small_orange_diamond:.

Une fonction membre ne peut être appelée sur une instance[^înstance] constante si et seulement si celle-ci ne modifie pas l'objet.
Pour indiquer au compilateur que cette fonction ne modifie pas l'objet on ajoute `const` après sa liste de paramètres :small_blue_diamond:.

```cpp
class FirstClass {
    float _a;
public:
    float getA() const;
};
```

:arrow_forward: Modifiez les *getter* pour garantir qu'ils ne modifient pas l'instance.

Il est recommandé de mettre les fonctions membres en `const` dès que cela est possible (cf. [CoreGuidelines](https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines#Rconst-fct)).

Le second type d'accesseur est appelé *setter*.
Ils permettent de modifier une variable membre depuis l'extérieur et sont préfixés par `set`.

:arrow_forward: Ajoutez un setter par canal de couleur :small_orange_diamond:.

---

[^înstance]: Une instance de classe est une variable dont les caractéristiques et le comportement sont définis par une classe.

## Initialisation

:arrow_forward: Lors de l'exécution, quel est l'état de la variable `colorScaleFilter`. :small_blue_diamond:

Pour donner l'état, d'un objet à sa création, on doit donner une valeur à ses variables membres :small_blue_diamond:.
Pour ce faire, on crée une fonction membre spéciale qui permet d'indiquer comment l'objet doit être construit.

Le *constructeur* possède le même nom que la classe et n'a pas de type de retour.

```cpp
class FirstClass {
    FirstClass();
};
```

Pour initialiser des variables membres dans un constructeur, on utilise la liste d'initialisation. Les variables membres qui y sont initialisées doivent l'être dans l'ordre où elles ont été déclarées :small_orange_diamond:.

```cpp
class FirstClass {
    int _a;
    int _b;
public:
    FirstClass(int a, int b);
};

FirstClass::FirstClass(int a, int b):
    _a(a),
    _b(b)
{}
```

:arrow_forward: Ajouter un constructeur avec trois paramètres afin d'initialiser les variables membres de `ColorScale` :small_orange_diamond:.

## La bibliothèque CImg

On utilise, dans le cadre du projet, la bibliothèque [CImg](https://cimg.eu/) qui permet la manipulation d'images en C++. Seules quelques unes de ces fonctionnalités nous intéressent : manipulation des pixels, interopérabilité des formats d'images.

:arrow_forward: Ajoutez à la classe `ColorScale` une méthode `apply` prenant en paramètre une image. On modifiera alors chaque pixel de l'image pour appliquer les multiplicateurs aux canaux de couleurs. 
